# Contribuer au projet

##  1. <a name='Sommaire'></a>Sommaire

<!-- vscode-markdown-toc -->
* 1. [Sommaire](#Sommaire)
* 2. [Contexte](#Contexte)
	* 2.1. [Machine](#Machine)
	* 2.2. [Information de lecture](#Informationdelecture)
* 3. [Environnement de développement](#Environnementdedveloppement)
	* 3.1. [Ubuntu Server](#UbuntuServer)
		* 3.1.1. [Installation](#Installation)
		* 3.1.2. [PostgreSQL](#PostgreSQL)
		* 3.1.3. [Création d'une clef SSH](#CrationduneclefSSH)
		* 3.1.4. [Cloner le repository](#Clonerlerepository)
	* 3.2. [Visual Studio Code](#VisualStudioCode)
		* 3.2.1. [Installation](#Installation-1)
		* 3.2.2. [Extensions](#Extensions)
		* 3.2.3. [Connexion à Ubuntu Server](#ConnexionUbuntuServer)
	* 3.3. [Environnement virtuel Python](#EnvironnementvirtuelPython)
		* 3.3.1. [Installations](#Installations)
		* 3.3.2. [Création de l'environnement virtuel](#Crationdelenvironnementvirtuel)
		* 3.3.3. [Installation des dépendancs](#Installationdesdpendancs)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  2. <a name='Contexte'></a>Contexte

###  2.1. <a name='Machine'></a>Machine

|Machine |OS  | Commentaire|
--- | --- | ---
|Hôte|Windows 10|Machine contenant VSCode|
|Serveur|Ubuntu Server|Machine recevant le code source|

###  2.2. <a name='Informationdelecture'></a>Information de lecture

Les commandes sur la machine hôte sont executées en PowerShell et ont le prefixe suivant (où `pev` correspond à l'utilisateur et `win` au nom de la machine Windows):
```powershell
pev@win >
```

Les commande sur la machine serveur sont executées en Bash et ont le préfixe suivant (où `pev` correspond à l'utilisateur et `ubu` au nom de la machine Ubuntu Server):
```bash
pev@ubu $
```

---

##  3. <a name='Environnementdedveloppement'></a>Environnement de développement

###  3.1. <a name='UbuntuServer'></a>Ubuntu Server

####  3.1.1. <a name='Installation'></a>Installation

Depuis le site https://ubuntu.com/download/server télécharger l'ISO d'Ubuntu Server20.04.2. L'installer sur une machine virtuelle (dans notre cas VMWare). Sélectionner les options qui conviennent à votre machine, puis cocher l'option OpenSSH pour obtenir un accès SSH au serveur.

Redémarrer le serveur. Puis se loguer une première fois.

Mettre à jour le serveur
```bash
pev@ubu ~ $ sudo apt-get update
pev@ubu ~ $ sudo apt-get upgrade
```

Démarrer le serveur à chaque nouvelle session de développement.

####  3.1.2. <a name='PostgreSQL'></a>PostgreSQL

Installer le le paquet pour Ubuntu Server
```bash
pev@ubu ~ $ sudo apt-get install postgresql postgresql-contrib
```

Installer quelques lib complémentaires pour Linux
```bash
pev@ubu ~ $ sudo apt-get install libpq-dev python3-dev
```

Confirmer l'accès à l'instance PostgreSQL
```bash
pev@ubu ~ $  sudo -i -u postgres
postgres@ubu ~ $ psql
psql (12.6 (Ubuntu 12.6-0ubuntu0.20.10.1))
Type "help" for help.

postgres=#
```

Créer la base de données qui recevra les modèles django, ici `db` et l'utilisateur principal, ici `pev`
```sql
postgres=# -- Create database
postgres=# CREATE DATABASE db;

postgres=# -- Create user
postgres=# CREATE USER pev WITH ENCRYPTED PASSWORD 'simple';

postgres=# ALTER ROLE pev SET client_encoding TO 'utf8';
postgres=# ALTER ROLE pev SET default_transaction_isolation TO 'read committed';
postgres=# ALTER ROLE pev SET timezone TO 'UTC';

postgres=# -- Set db to user
postgres=# GRANT ALL PRIVILEGES ON DATABASE db TO pev;
```

Quitter
```bash
postgres=# \q
postgres@ubu ~ $ exit
pev@ubu ~ $ 
```

Configurer les accès exterieurs en modifiant le `ph_hba.conf`
- authoriser les accès localhost `trust`
- connexion `md5`pour les adresse du réseau `192.168.146.0`
```bash
pev@ubu ~ $ vim /etc/postgresql/12/main/pg_hba.conf
```
```
# IPv4 local connections:
host    all             all             127.0.0.1/32            trust
host    all             all             192.168.146.0/32        md5
```

Redémarrer le service postgresql
```bash
pev@ubu ~ $ service postgresql restart
```


####  3.1.3. <a name='CrationduneclefSSH'></a>Création d'une clef SSH

Créer une clef SSH pour l'utilisateur connecté.
```bash
pev@ubu ~ $ ssh-keygen -t ed25519 -C "<comment>"
```

Copier le contenu de la clef publique dans Gitlab
```bash
pev@ubu ~ $ cat ~/.ssh/id_ed25519.pub
```

####  3.1.4. <a name='Clonerlerepository'></a>Cloner le repository

Se créer un répertoire de travail, puis y cloner les sources dedans
```bash
pev@ubu ~ $ mkdir ~/work
pev@ubu ~ $ cd ~/work
pev@ubu work $ git clone git@gitlab.com:PapyPev/airdrink.git
```



###  3.2. <a name='VisualStudioCode'></a>Visual Studio Code

####  3.2.1. <a name='Installation-1'></a>Installation

Installer Visual Studio Code depuis l'exécutable du site de Microsoft : https://code.visualstudio.com/Download , puis suivre l'installation guidée.

####  3.2.2. <a name='Extensions'></a>Extensions

Extensions obligatoires :
- Python
- Remote - SSH

Extensions recommandées :
- Python
- Code Spell Checker
- Dash
- EditorConfig for VS Code
- Git Graph
- GitLens
- JSON Tools
- Markdown TOC
- Pylance
- Python Docstring Generator
- python-snippets
- Rewrap
- Trailing Spaces

####  3.2.3. <a name='ConnexionUbuntuServer'></a>Connexion à Ubuntu Server

Une fois le serveur Ubuntu Server démarré, rechercher l'IP de la machine, ici : `192.168.146.130`
```bash
pev@ubu ~ $ ifconfig
```
```
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.146.130  netmask 255.255.255.0  broadcast 192.168.146.255
        inet6 fe80::20c:29ff:fe28:3803  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:28:38:03  txqueuelen 1000  (Ethernet)
        RX packets 50904  bytes 55653435 (55.6 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 15718  bytes 2963574 (2.9 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 25505  bytes 3786537 (3.7 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 25505  bytes 3786537 (3.7 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

Ouvrir VSCode sur la machine Hôte et s'y connecter via Remote-SSH : https://code.visualstudio.com/docs/remote/ssh-tutorial
- Ajouter le serveur en tant que nouveau host `ssh pev@192.168.146.130`
- Ouvrir un VSCode sur le serveur fraichement ajouté
- Sélectionner le répertoire de travail dans `/home/pev/work/airdrink`

L'environnement Hôte est prêt.


###  3.3. <a name='EnvironnementvirtuelPython'></a>Environnement virtuel Python

####  3.3.1. <a name='Installations'></a>Installations

Le Python par défaut d'Ubuntu Server est Python 3.8.6 (default, Jan 27 2021, 15:42:20). Il n'est pas nécessaire d'installer une version supplémentaire.

Installer le gestionnaire d'environnement virtuel Python `virtualenv` via :
```bash
pev@ubu ~ $ sudo apt-get install python3-virtualenv
```

Vérifier l'installation via
```bash
pev@ubu ~ $ virtualenv --version
virtualenv 20.0.29+ds from /usr/lib/python3/dist-packages/virtualenv/__init__.py
```

####  3.3.2. <a name='Crationdelenvironnementvirtuel'></a>Création de l'environnement virtuel

Créer l'environnement virtuel `venv` dans les sources du repo
```bash
pev@ubu ~ $ cd ~/work/airdrink
pev@ubu airdrink $ virtualenv ~/work/airdrink/venv
```

Activer l'environnement virtuel, un prefixe `(venv)` se trouve en début de prompt pour confirmer le chargement de l'environnement virtuel
```bash
pev@ubu airdrink $ source venv/bin/activate
(venv) pev@ubu:~/work/airdrink$
```

Activer l'environnement virtuel pour toute opération de développement


####  3.3.3. <a name='Installationdesdpendancs'></a>Installation des dépendancs

Installer les dépendances requises
```bash
(venv) pev@ubu airdrink $ pip install -r requirements-dev.txt
```

Installer pre-commit si ce n'est pas encore fait
```bash
(venv) pev@ubu airdrink $ pre-commit install
```

S'assurer que Django fonctionne, dans notre cas, Django 3.2.0
```bash
(venv) pev@ubu airdrink $ python
Python 3.8.6 (default, Jan 27 2021, 15:42:20)
[GCC 10.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import django
>>> django.VERSION
(3, 2, 0, 'final', 0)
```

S'assurer que Django démarre, en spécifiant l'IP de sa machine comme argument
```bash
(venv) pev@ubu:~/work/airdrink/airdrink $ python manage.py runserver 192.168.146.130:8000
...
```
La page d'accueil doit être joignable sur l'url : http://192.168.146.130:8000/

Puis quitter avec ctrl+c

