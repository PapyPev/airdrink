# Airdrink

[![pipeline status](https://gitlab.com/PapyPev/airdrink/badges/master/pipeline.svg)](https://gitlab.com/PapyPev/airdrink/-/commits/master)
[![coverage report](https://gitlab.com/PapyPev/airdrink/badges/master/coverage.svg)](https://gitlab.com/PapyPev/airdrink/-/commits/master)
[![code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Application django de gestion de cocktails.

- [Environnement et contribution](CONTRIBUTING.md)
- [Script d'intégration de données](airdrink/)
- [Schéma de base de données](doc/database.png)
- [Application django](data/)

## Commandes importantes

Commandes à executer qu'une fois la machine opérationnelle.

Ajouter l'IP de la machine dans le fichier `settings.py`
```python
ALLOWED_HOSTS = [
    "192.168.146.130"
]
```

Configurer la connexion à la base de données en modifiant la variable `DATABASES` dans le fichier `settings.py`
```python
#...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'db',
        'USER': 'pev',
        'PASSWORD': 'simple',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
#...
```

Préparer les modèles django pour la base de données
```bash
$ python manage.py makemigrations drinks
```

Appliquer les modèles django à la base de données
```bash
$ python manage.py migrate
```

Démarrer le serveur django sur l'IP `192.168.146.130` et le port `8000`, quitter par Ctrl+C:
```bash
$ python manage.py runserver 192.168.146.130:8000
```

Lancer les contrôles qualités (sort, pep8, linter et duck typing) avec `pre-commit` depuis la racine du repo:
```
$ pre-commit run --all-files
```


## TODO

- proposer des cocktails similaires
- importer des données en mise à jour
- tests unitaires
- tests fonctionnels
- CI


## Si c'était à refaire

- usage d'un cookiecutter https://github.com/pydanny/cookiecutter-django
- usage de tox dès le début
- directement depuis les tests unitaires
