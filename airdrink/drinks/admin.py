# coding=utf-8

# =============================================================================
# IMPORT
# =============================================================================

from django.contrib import admin

from .models.Alcoholic import Alcoholic
from .models.Category import Category
from .models.Drink import Drink
from .models.Glass import Glass
from .models.Ingredient import Ingredient
from .models.Recipe import Recipe

# =============================================================================
# REGISTER
# =============================================================================

# Register your models here.
admin.site.register(Alcoholic)
admin.site.register(Category)
admin.site.register(Drink)
admin.site.register(Glass)
admin.site.register(Ingredient)
admin.site.register(Recipe)
