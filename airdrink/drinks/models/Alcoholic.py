# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from django.db import models

# =============================================================================
# MODEL
# =============================================================================


class Alcoholic(models.Model):
    """docstring for Alcoholic."""

    alcoholic = models.CharField(max_length=255)

    class Meta:
        ordering = ["alcoholic"]

    def __str__(self):
        return self.alcoholic
