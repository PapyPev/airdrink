# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from django.db import models

# =============================================================================
# MODEL
# =============================================================================


class Category(models.Model):
    """docstring for Category."""

    category = models.CharField(max_length=255)

    class Meta:
        ordering = ["category"]

    def __str__(self):
        return self.category
