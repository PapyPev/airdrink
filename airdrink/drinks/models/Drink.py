# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from django.db import models
from drinks.models.Alcoholic import Alcoholic
from drinks.models.Category import Category
from drinks.models.Glass import Glass

# =============================================================================
# MODEL
# =============================================================================


class Drink(models.Model):
    """docstring for Drink."""

    id_drink = models.CharField(max_length=255)
    drink = models.CharField(max_length=255)
    date_modified = models.DateTimeField(null=True, auto_now=True)
    fk_alcoholic = models.ForeignKey(
        Alcoholic,
        models.SET_NULL,
        blank=True,
        null=True,
    )
    fk_category = models.ForeignKey(
        Category,
        models.SET_NULL,
        blank=True,
        null=True,
    )
    thumbnail = models.TextField(null=True)
    fk_glass = models.ForeignKey(
        Glass,
        models.SET_NULL,
        blank=True,
        null=True,
    )
    iba = models.TextField(null=True)
    instructions = models.TextField(null=True)
    video = models.TextField(null=True)

    class Meta:
        ordering = ["drink"]

    def __str__(self):
        return self.drink
