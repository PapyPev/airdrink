# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from django.db import models

# =============================================================================
# MODEL
# =============================================================================


class Glass(models.Model):
    """docstring for Glass."""

    glass = models.CharField(max_length=255)

    class Meta:
        ordering = ["glass"]

    def __str__(self):
        return self.glass
