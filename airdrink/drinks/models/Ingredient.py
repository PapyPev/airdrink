# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from django.db import models

# =============================================================================
# MODEL
# =============================================================================


class Ingredient(models.Model):
    """docstring for Ingredient."""

    ingredient = models.CharField(max_length=255)

    class Meta:
        ordering = ["ingredient"]

    def __str__(self):
        return self.ingredient
