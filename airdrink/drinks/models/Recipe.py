# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from django.db import models
from drinks.models.Drink import Drink
from drinks.models.Ingredient import Ingredient

# =============================================================================
# MODEL
# =============================================================================


class Recipe(models.Model):
    """docstring for Recipe."""

    fk_drink = models.ForeignKey(Drink, on_delete=models.CASCADE)
    fk_ingredient = models.ForeignKey(
        Ingredient,
        models.SET_NULL,
        blank=True,
        null=True,
    )
    measure = models.CharField(max_length=255, null=True)
