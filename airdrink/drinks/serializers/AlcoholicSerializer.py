# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Alcoholic import Alcoholic
from rest_framework import serializers

# =============================================================================
# SERIALIZER
# =============================================================================


class AlcoholicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alcoholic
        fields = "__all__"
