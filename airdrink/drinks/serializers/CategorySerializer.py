# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Category import Category
from rest_framework import serializers

# =============================================================================
# SERIALIZER
# =============================================================================


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"
