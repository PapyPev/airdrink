# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Drink import Drink
from rest_framework import serializers

# =============================================================================
# SERIALIZER
# =============================================================================


class DrinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drink
        fields = "__all__"
