# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Glass import Glass
from rest_framework import serializers

# =============================================================================
# SERIALIZER
# =============================================================================


class GlassSerializer(serializers.ModelSerializer):
    class Meta:
        model = Glass
        fields = "__all__"
