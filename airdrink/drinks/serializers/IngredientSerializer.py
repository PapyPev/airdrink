# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Ingredient import Ingredient
from rest_framework import serializers

# =============================================================================
# SERIALIZER
# =============================================================================


class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = "__all__"
