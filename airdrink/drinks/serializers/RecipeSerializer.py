# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Recipe import Recipe
from rest_framework import serializers

# =============================================================================
# SERIALIZER
# =============================================================================


class RecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recipe
        fields = "__all__"
