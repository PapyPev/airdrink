from django.test import TestCase


class ViewsTestCase(TestCase):
    def test_index_loads_properly(self):
        """The index page loads properly"""
        response = self.client.get("http://192.168.146.130:8000/")
        self.assertEqual(response.status_code, 200)
