# -*- coding=utf-8 -*-

# =============================================================================
# IMPORTS
# =============================================================================

from django.urls import include, path
from drinks.views import (
    AlcoholicViewSet,
    CategoryViewSet,
    DrinkViewSet,
    GlassViewSet,
    IngredientViewSet,
    RecipeViewSet,
)
from rest_framework import routers

# =============================================================================
# ROUTES
# =============================================================================

router = routers.DefaultRouter()
router.register(r"alcoholic", AlcoholicViewSet, basename="alcoholic")
router.register(r"category", CategoryViewSet, basename="category")
router.register(r"drink", DrinkViewSet, basename="drink")
router.register(r"glass", GlassViewSet, basename="glass")
router.register(r"ingredient", IngredientViewSet, basename="ingredient")
router.register(r"recipe", RecipeViewSet, basename="recipe")

urlpatterns = [
    path("", include(router.urls)),
]
