# coding=utf-8

# =============================================================================
# MODULE IMPORTS - VIEWS - API
# =============================================================================

from drinks.views.api.AlcoholicViewSet import AlcoholicViewSet
from drinks.views.api.CategoryViewSet import CategoryViewSet
from drinks.views.api.DrinkViewSet import DrinkViewSet
from drinks.views.api.GlassViewSet import GlassViewSet
from drinks.views.api.IngredientViewSet import IngredientViewSet
from drinks.views.api.RecipeViewSet import RecipeViewSet
from drinks.views.www.DrinkRecipeView import DrinkRecipeView
from drinks.views.www.index import index
from drinks.views.www.SearchDrinksView import SearchDrinksView

# =============================================================================
# MODULE IMPORTS - VIEWS - WWW
# =============================================================================
