# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Alcoholic import Alcoholic
from drinks.serializers import AlcoholicSerializer
from rest_framework import viewsets

# =============================================================================
# MODEL
# =============================================================================


class AlcoholicViewSet(viewsets.ViewSet):
    queryset = Alcoholic.objects.all()
    serializer_class = AlcoholicSerializer
