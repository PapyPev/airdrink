# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Category import Category
from drinks.serializers import CategorySerializer
from rest_framework import viewsets

# =============================================================================
# MODEL
# =============================================================================


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
