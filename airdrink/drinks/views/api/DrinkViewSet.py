# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Drink import Drink
from drinks.serializers import DrinkSerializer
from rest_framework import viewsets

# =============================================================================
# MODEL
# =============================================================================


class DrinkViewSet(viewsets.ModelViewSet):
    queryset = Drink.objects.all()
    serializer_class = DrinkSerializer
