# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Glass import Glass
from drinks.serializers import GlassSerializer
from rest_framework import viewsets

# =============================================================================
# MODEL
# =============================================================================


class GlassViewSet(viewsets.ModelViewSet):
    queryset = Glass.objects.all()
    serializer_class = GlassSerializer
