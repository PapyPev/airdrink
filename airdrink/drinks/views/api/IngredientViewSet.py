# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Ingredient import Ingredient
from drinks.serializers import IngredientSerializer
from rest_framework import viewsets

# =============================================================================
# MODEL
# =============================================================================


class IngredientViewSet(viewsets.ModelViewSet):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer
