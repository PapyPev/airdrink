# coding=utf-8

# =============================================================================
# IMPORTS
# =============================================================================


from drinks.models.Recipe import Recipe
from drinks.serializers import RecipeSerializer
from rest_framework import viewsets

# =============================================================================
# MODEL
# =============================================================================


class RecipeViewSet(viewsets.ModelViewSet):
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer
