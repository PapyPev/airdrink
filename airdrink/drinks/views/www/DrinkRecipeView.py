# coding=utf-8

# =============================================================================
# IMPORT
# =============================================================================


from django.views.generic import DetailView
from drinks.models.Drink import Drink

# =============================================================================
# VIEW
# =============================================================================


class DrinkRecipeView(DetailView):
    model = Drink
    template_name = "recipe.html"
