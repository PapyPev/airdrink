# coding=utf-8

# =============================================================================
# IMPORT
# =============================================================================


from django.db.models import Q
from django.views.generic.list import ListView
from drinks.models.Drink import Drink

# =============================================================================
# VIEW
# =============================================================================


class SearchDrinksView(ListView):
    model = Drink
    template_name = "search.html"

    def get_queryset(self, *args, **kwargs):
        query = self.request.GET.get("q")
        if query is not None:
            return Drink.objects.filter(
                Q(drink__icontains=query)
                | Q(recipe__fk_ingredient__ingredient__icontains=query)
            ).distinct()
        else:
            return Drink.objects.all()
