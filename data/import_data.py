# coding=utf-8
"""import data from all_drinks.csv to postgresql db.public
"""

# =============================================================================
# IMPORT
# =============================================================================

import csv
import datetime
import logging
import os
import sys
from typing import List

import psycopg2

# =============================================================================
# PARAMETERS
# =============================================================================


DB_HOSTNAME = "localhost"
"""str: PostgreSQL host name"""

DB_DATABASE = "db"
"""str: PostgreSQL database name"""

DB_USERNAME = "pev"
"""str: PostgreSQL username"""

DB_PASSWORD = "simple"
"""str: PostgreSQL user's password"""

DB_PORT = "5432"
"""str: PostgreSQL datbase running port"""


# =============================================================================
# CONSTANTS
# =============================================================================


START_DT = datetime.datetime.now()
"""datetime.datetime.date: start timestamp of this script"""

CURRENT_DIRPATH = ""
"""str: The current directory of the script"""
if getattr(sys, "frozen", False):
    CURRENT_DIRPATH = os.path.dirname(sys.executable)
elif __file__:
    CURRENT_DIRPATH = os.path.dirname(__file__)

CSV_DATA_FILEPATH = os.path.join(CURRENT_DIRPATH, "all_drinks.csv")
"""str: The all_drinks.csv data filepath"""
if not os.path.exists(CSV_DATA_FILEPATH):
    raise ValueError(f"CSV file '{CSV_DATA_FILEPATH}' is missing")

LOG_DIRPATH = os.path.join(CURRENT_DIRPATH, "log")
"""str: The log directory path"""
if not os.path.exists(LOG_DIRPATH):
    os.makedirs(LOG_DIRPATH)

LOG_FILEPATH = os.path.join(
    LOG_DIRPATH, "import_data_{}.log".format(START_DT.strftime("%Y%m%d_%H%M%S"))
)
"""str: The current log filepath"""


# =============================================================================
# FUNCTIONS
# =============================================================================

# -----------------------------------------------------------------------------


def __is_value_exists_in_table(
    cur: psycopg2.extensions.cursor, table_name: str, field_name: str, value: str
):
    """Check if `value` exists in `field_name` from table `table_name`

    Parameters
    ----------
    cur: cur: psycopg2.extensions.cursor
        The cursor object in database
    table_name : str
        Name of the database table
    field_name : str
        The field's name to control
    value : str
        The value to search for

    Returns
    -------
    bool
        True if value already exists, False otherwise
    """
    query = """SELECT {} FROM {} WHERE {} = %(feature)s;""".format(
        field_name, table_name, field_name
    )
    args = {"feature": value}
    cur.execute(query, args)
    return cur.fetchone() is not None


# -----------------------------------------------------------------------------


def __insert_drink_and_recipe(cur: psycopg2.extensions.cursor, data: dict):

    drink_table_name = "db.public.drinks_drink"
    recipe_table_name = "db.public.drinks_recipe"

    drink_attributes = list(data[0].keys())
    drink_attributes.remove("__ingredients_and_measures")
    recipe_attributes = ["fk_drink_id", "fk_ingredient_id", "measure"]

    query_drink = """INSERT INTO {} ({}) VALUES ({}) RETURNING id;""".format(
        drink_table_name,
        ",".join(x for x in drink_attributes),
        ",".join(f"%({x})s" for x in drink_attributes),
    )
    query_recipe = """INSERT INTO {} ({}) VALUES ({});""".format(
        recipe_table_name,
        ",".join(x for x in recipe_attributes),
        ",".join(f"%({x})s" for x in recipe_attributes),
    )

    for drink_index, drink in enumerate(data, start=1):

        if not __is_value_exists_in_table(
            cur=cur,
            table_name=drink_table_name,
            field_name="id_drink",
            value=drink.get("id_drink"),
        ):

            ingredients_measures = drink.get("__ingredients_and_measures", [])
            drink.pop("__ingredients_and_measures")

            logging.info(
                f"Import {drink_table_name} {drink_index}/{len(data)} -> {drink['drink']} -> insert"
            )
            cur.execute(query_drink, drink)
            new_drink_id = cur.fetchone()[0]

            logging.info(f"Import {recipe_table_name} for {drink['drink']}")
            for ingredient_measure_index, ingredient_measure in enumerate(
                ingredients_measures, start=1
            ):
                ingredient_measure["fk_drink_id"] = new_drink_id
                cur.execute(query_recipe, ingredient_measure)

        else:
            logging.warning(
                f"Import {drink_table_name} {drink_index}/{len(data)} -> {drink['drink']} with [{drink['id_drink']}] already exists"
            )


# -----------------------------------------------------------------------------


def __insert_row_into_database_table(
    cur: psycopg2.extensions.cursor, table_name: str, field_name: str, data: list
) -> dict:
    """Adds in the `table_name` the values of `data` in the only `field_name` of the table

    Parameters
    ----------
    cur: cur: psycopg2.extensions.cursor
        The cursor object in database
    table_name : str
        Name of the database table to populate
    field_name : str
        The field's name to populate
    data : list
        A list of feature containing feature to add

    Returns
    -------
    list
        A list of dict with `data` value as key and the generated id as valueself.
        Example:
        >>> results = __insert_row_into_database_table(...)
        >>> print(results)
        [{'my inserted value': 3}, {'my other inserted value': 4}]
    """
    logging.info(f"Import {table_name}")
    results = {}
    query = """INSERT INTO {} ({}) VALUES (%(field_name)s) RETURNING id;""".format(
        table_name, field_name
    )
    for feature_index, feature in enumerate(data, start=1):

        if not __is_value_exists_in_table(
            cur=cur, table_name=table_name, field_name=field_name, value=feature
        ):
            logging.info(
                f"Import {table_name} {feature_index}/{len(data)} -> {feature} -> insert"
            )
            cur.execute(query, {"field_name": feature})
            new_id = cur.fetchone()[0]
            results[feature] = new_id
        else:
            logging.warning(
                f"Import {table_name} {feature_index}/{len(data)} -> {feature} -> already exists"
            )

    return results


# -----------------------------------------------------------------------------


def __get_drinks_and_recipe(
    data: list,
    data_drink_alcoholics: dict,
    data_drink_categories: dict,
    data_drink_ingredients: dict,
    data_drink_glasses: dict,
) -> list:
    """Get all drinks and their recipe

    Parameters
    ----------
    data : list
        List of dict drink representation from CSV row
    data_drink_alcoholics : dict
        A dict of alcoholic attributes {'value': 'database_id'}
    data_drink_categories : dict
        A dict of categorie attributes {'value': 'database_id'}
    data_drink_ingredients : dict
        A dict of ingredient attributes {'value': 'database_id'}
    data_drink_glasses : dict
        A dict of glass attributes {'value': 'database_id'}

    Returns
    -------
    list
        A list of dict representation of drinks and their recipes.
        Recipe is store in attribute '__ingredients_and_measures', containing ingredient's id in database and measure
    """
    drinks_and_recipe = []
    for row in data:

        ingredients_measures = []
        for ingredient_index in range(0, 16):
            ingredient_column_name = f"strIngredient{ingredient_index}"
            measure_column_name = f"strMeasure{ingredient_index}"
            ingredient_id = data_drink_ingredients.get(
                row.get(ingredient_column_name), None
            )
            measure = row.get(measure_column_name, None)
            measure = None if not measure else measure
            if ingredient_id is not None and ingredient_id:
                ingredients_measures.append(
                    {"fk_ingredient_id": ingredient_id, "measure": measure}
                )

        drink_and_recipe = {
            "id_drink": row.get("idDrink", None),
            "drink": row.get("strDrink", None),
            "date_modified": row.get("dateModified", None)
            if row.get("dateModified", None)
            else None,
            "fk_alcoholic_id": data_drink_alcoholics.get(
                row.get("strAlcoholic", None), None
            ),
            "fk_category_id": data_drink_categories.get(
                row.get("strCategory", None), None
            ),
            "thumbnail": row.get("strDrinkThumb", None),
            "fk_glass_id": data_drink_glasses.get(row.get("strGlass", None), None),
            "iba": row.get("strIBA", None),
            "instructions": row.get("strInstructions", None),
            "video": row.get("strVideo", None),
        }
        drink_and_recipe["__ingredients_and_measures"] = ingredients_measures
        drinks_and_recipe.append(drink_and_recipe)

    return drinks_and_recipe


# -----------------------------------------------------------------------------


def __get_unique_ingredients(data: list) -> List[str]:
    """Return unique values from all ingredients columns

    Parameters
    ----------
    data : list
        dict reprezentation of a row in CSV

    Returns
    -------
    List[str]:
        A list of unique values from all ingredients columns
    """
    unique_drink_ingredients = []

    # Detect strIngredient columns
    columns = (
        [k for k in data[0].keys() if k.startswith("strIngredient")] if data else []
    )

    for ingredient_column_name in columns:
        unique_drink_ingredients_index = __get_unique_values(
            __get_unique_values_from_column(data, ingredient_column_name)
        )
        unique_drink_ingredients = list(
            set(unique_drink_ingredients + unique_drink_ingredients_index)
        )

    return unique_drink_ingredients


# -----------------------------------------------------------------------------


def __get_unique_values(data: list) -> List[str]:
    """Flatten a list of data

    Parameters
    ----------
    data : list
        List of data to flat

    Returns
    -------
    List[str]
        A list of unique values
    """
    return [_ for _ in list(set(data)) if _]


# -----------------------------------------------------------------------------


def __get_unique_values_from_column(data: List[dict], key: str) -> List[str]:
    """Return all column values in CSV list

    Parameters
    ----------
    data : List[dict]
        List of dict representation of CSV row
    key : str
        The column name, or the key in list

    Returns
    -------
    List[str]
        A list of all unique values from column
    """
    column_values = []
    for row in data:
        column_values.append(row.get(key, None))
    return column_values


# -----------------------------------------------------------------------------


def import_data(data: list):
    logging.info("Import data into PostgreSQL")

    # Get all uniques values
    unique_drink_alcoholics = __get_unique_values(
        __get_unique_values_from_column(data, "strAlcoholic")
    )
    unique_drink_categories = __get_unique_values(
        __get_unique_values_from_column(data, "strCategory")
    )
    unique_drink_glasses = __get_unique_values(
        __get_unique_values_from_column(data, "strGlass")
    )
    unique_drink_ingredients = __get_unique_ingredients(data)

    # Log this
    logging.debug(
        "Unique values of strAlcoholic: {}".format(len(unique_drink_alcoholics))
    )
    logging.debug(
        "Unique values of strCategory: {}".format(len(unique_drink_categories))
    )
    logging.debug("Unique values of strGlass: {}".format(len(unique_drink_glasses)))
    logging.debug(
        "Unique values of strIngredient: {}".format(len(unique_drink_ingredients))
    )

    # Connect to database
    db_conn = psycopg2.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        host=DB_HOSTNAME,
        port=DB_PORT,
        database=DB_DATABASE,
    )

    # Get cursor
    cur = db_conn.cursor()

    # Import uniques
    data_drink_alcoholics = __insert_row_into_database_table(
        cur=cur,
        table_name="db.public.drinks_alcoholic",
        field_name="alcoholic",
        data=unique_drink_alcoholics,
    )
    data_drink_categories = __insert_row_into_database_table(
        cur=cur,
        table_name="db.public.drinks_category",
        field_name="category",
        data=unique_drink_categories,
    )
    data_drink_glasses = __insert_row_into_database_table(
        cur=cur,
        table_name="db.public.drinks_glass",
        field_name="glass",
        data=unique_drink_glasses,
    )
    data_drink_ingredients = __insert_row_into_database_table(
        cur=cur,
        table_name="db.public.drinks_ingredient",
        field_name="ingredient",
        data=unique_drink_ingredients,
    )

    drinks_and_recipe = __get_drinks_and_recipe(
        data,
        data_drink_alcoholics,
        data_drink_categories,
        data_drink_ingredients,
        data_drink_glasses,
    )
    __insert_drink_and_recipe(cur=cur, data=drinks_and_recipe)

    # Save only if success
    db_conn.commit()


# -----------------------------------------------------------------------------


def read_csv() -> list:
    """Read data in all_drinks.csv file

    Returns
    -------
    list
        A list of dict representing the drink information contained in the CSV file
    """
    logging.info("Read CSV file")
    data = []
    with open(CSV_DATA_FILEPATH, mode="r", encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile)
        for line in reader:
            data.append(line)
    return data


# -----------------------------------------------------------------------------


def set_logging():
    """Set logging configuration"""

    log_format = "%(asctime)s | %(levelname)s\t | %(message)s"
    log_level = logging.DEBUG

    # Root logger
    root = logging.getLogger()

    # Clear handlers
    if root.hasHandlers():
        root.handlers = []

    # Default level and formatter
    root.setLevel(log_level)
    formatter = logging.Formatter(fmt=log_format)

    # File handler
    file_handler = logging.FileHandler(filename=LOG_FILEPATH)
    file_handler.setFormatter(formatter)
    root.addHandler(file_handler)

    # Console Handler
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    root.addHandler(console_handler)


# -------------------------------------------------------------------------


def main():
    """Main runner"""
    set_logging()

    logging.info("=" * 80)
    logging.info("START DRINK DATA")
    logging.info("=" * 80)

    try:
        csv_data = read_csv()
        import_data(data=csv_data)
    except Exception as e:
        logging.critical(e, exc_info=True)

    logging.info("=" * 80)
    logging.info("END DRINK DATA")
    logging.info(f"Duration: {str(datetime.datetime.now() - START_DT)}")
    logging.info("=" * 80)


# =============================================================================
# MAIN
# =============================================================================


if __name__ == "__main__":
    main()
